# MapAPI
本repo為MAP的後端API，詳細專案內容請前往前端repo

* 原始碼
    * 前端網頁：https://gitlab.com/demograpicdatamap/map
    * 後端API：https://gitlab.com/demograpicdatamap/mapapi

* Demo網址（2024/01/06前有效）： https://www.blackboard.services/