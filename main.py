from typing import Optional
from fastapi import FastAPI, Depends
from mysql.connector.cursor import MySQLCursor
from routers.db import get_db, db_database
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/api/{table}/{city}")
def get_cities(table:Optional[str], city: Optional[str] = None, year: Optional[int] = None, db: MySQLCursor = Depends(get_db)):

    if year:
        Year = f"Year{year}"

    else:
        Year = "*"
    
    query = f"SELECT {Year} FROM {db_database}.{table}"

    conditions = []

    if city:
        conditions.append(f" WHERE Region = '{city}'")

    if conditions:
        query += " AND ".join(conditions)

    print(query)


    db.execute(query)
    result = db.fetchall()
    
    if result:
        print(result)
        data = [item for item in result[0] if item != city]
        return data
    else:
        return {"error": f"Data not found for table {table} with specified conditions"}
    
@app.get("/api/{table}")
def get_tables(table:Optional[str], city: Optional[str] = None, year: Optional[int] = None, db: MySQLCursor = Depends(get_db)):

    if year:
        Year = f"Year{year}"

    else:
        Year = "*"
    
    query = f"SELECT {Year} FROM {db_database}.{table}"

    conditions = []

    if city:
        conditions.append(f" WHERE Region = '{city}'")

    if conditions:
        query += " AND ".join(conditions)

    print(query)


    db.execute(query)
    result = db.fetchall()
    
    if result:
        print(result)
        data = []
        for item in result:
            cleaned_item = [value.replace('\r', '') for value in item[1:]]
            data.append(cleaned_item)
        

        return data
    else:
        return {"error": f"Data not found for table {table} with specified conditions"}