from mysql.connector import Error
import mysql.connector
from dotenv import load_dotenv
import os

# 加载 .env 文件中的配置
load_dotenv()

# 获取配置项的值
db_host = os.getenv("DB_HOST")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
db_database = os.getenv("DB_DATABASE")

def get_db_connection():
    connection = None  # Initialize the connection variable
    try:
        # 連接 MySQL/MariaDB 資料庫
        connection = mysql.connector.connect(
            host=db_host,          # 主機名稱
            database=db_database,      # 資料庫名稱
            user=db_user,                     # 帳號
            password=db_password,           # 密碼
            connect_timeout=30)           

        if connection.is_connected():
            # 顯示資料庫版本
            db_Info = connection.get_server_info()
            print("資料庫版本：", db_Info)

            # 顯示目前使用的資料庫
            cursor = connection.cursor()
            cursor.execute("SELECT DATABASE();")
            record = cursor.fetchone()
            print("目前使用的資料庫：", record)

    except Error as e:
        print("資料庫連接失敗：", e)
            
    return connection  # Return the connection variable

def get_db():
    connection = get_db_connection()
    db = connection.cursor()

    try:
        yield db
    finally:
        db.close()
        connection.close()
